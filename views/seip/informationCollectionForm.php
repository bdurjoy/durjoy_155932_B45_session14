<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Information Collection Form</title>
    <style>
        .formClass{
            width: 250px;
            height: 300px;
            margin: 0 auto;
            border: 5px solid darkslategrey;
            padding: 10px 10px;
            border-radius: 7px;
        }
        input{
            margin-top:15px;
        }
    </style>
</head>
</html>
<body>
<div class="formClass">
    <form action="process.php" method="post">
        Please Enter Your Name:
        <br>
        <input type="text" name="userName">
        <br>
        Please Enter Your StudentID:
        <br>
        <input type="text" name="studentID">
        <br>

        Please Enter Your Date of Birth:
        <br>
        <input type="date" name="dateOfBirth">
        <br>
        <input type="submit" value="Enter">
    </form>
</div>

</body>